import { Injectable } from '@angular/core';
import {Photo} from '../model/photo';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {Storage} from '@ionic/storage';
import {Alert} from "selenium-webdriver";

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  public photos: Photo[] = [];
  public photosToShow: Array<Photo> = [];
  constructor(private camera: Camera, private storage: Storage) { }
  takePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    return this.camera.getPicture(options).then((imageData) => {
      this.photos.unshift({
        data: 'data:image/jpeg;base64,' + imageData,
      });
      this.photosToShow.unshift({
        data: 'data:image/jpeg;base64,' + imageData,
      });
      this.storage.set('photos', this.photos).then(() => {
          this.storage.set('photosToShow', this.photosToShow).then(() => {
            this.loadPicturesToShow().then();
          });
      });
    }, (err) => {
      // Handle error
      console.log('Camera issue:' + err);
    });
  }
  hidePhoto(src: Photo) {
    if (this.photosToShow.length === 1) {
      this.photosToShow = [];
    } else {
      this.photosToShow.forEach((photo, index) => {
        if (photo === src) {
          this.photosToShow.splice(index, 1);
        }
      });
      return this.storage.set('photosToShow', this.photosToShow).then(() => {
        this.loadPicturesToShow().then();
      });
    }
  }
  loadPictures() {
    return this.storage.get('photos').then((photos) => {
      this.photos = photos || [];
    });
  }
  loadPicturesToShow() {
    return this.storage.get('photosToShow').then((photos) => {
      this.photosToShow = photos || [];
    });
  }
  showAll() {
    this.loadPictures().then(() => {
      this.photos.forEach((photo, index) => {
        this.photosToShow[index] = photo;
      });
      this.storage.set('photosToShow', this.photosToShow).then(() => {
        this.loadPicturesToShow().then();
      });
    });
  }
  removeAll() {
    return this.storage.clear();
  }
}
