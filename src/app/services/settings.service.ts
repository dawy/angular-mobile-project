import { Injectable } from '@angular/core';
import {Record} from '../model/record';
import {Storage} from '@ionic/storage';
import {Photo} from '../model/photo';

@Injectable({
    providedIn: 'root'
})
export class SettingsService {
    private contacts: Array<string>;
    private messages: Array<string>;
    private actualMessages: Array<Array<{content: string, incoming: boolean, userPosition: number, position: number}>>;
    private background: Photo;
    records: Array<Record>;
    private time: {dHour: number, uHour: number, dMins: number, uMins: number};
    private messagesCount = 0;
    private messageText = '';
    constructor(private storage: Storage) {
        this.records = new Array<Record>();
        this.background = new Photo();
        this.time = {dHour: 0, uHour: 0, dMins: 0, uMins: 0};
        this.background.data = '';
        this.loadCorrespondence().then();
    }
    setMessageCount(count: number) {
        this.messagesCount = count;
    }
    setBackground(photo: Photo) {
        this.background = photo;
    }
    addContact(contact: string) {
        this.contacts.push(contact);
        this.addMessage({content: '', position: this.contacts.length - 1});
        this.actualMessages[this.contacts.length - 1] =
            new Array<{content: string, incoming: boolean, userPosition: number, position: number}>();
        this.saveState().then();
    }
    addMessage(data: {content: string, position: number}) {
        this.messages[data.position] = data.content;
        this.saveState().then();
    }
    setTime(time: {dHour: number, uHour: number, dMins: number, uMins: number}) {
        this.time = time;
    }
    addRecord(length) {
        const intLength =  parseInt(length, 10);
        this.records.push(new Record(intLength));
    }
    addActualMessage(data: {content: string, incoming: boolean, userPosition: number}) {
        this.actualMessages[data.userPosition].push({
            content: data.content,
            incoming: data.incoming,
            userPosition: data.userPosition,
            position: this.actualMessages[data.userPosition].length
        });
        this.messages[data.userPosition] = data.content;
        this.saveState().then();
    }
    changeSender(message: {content: string, incoming: boolean, userPosition: number, position: number}) {
        console.log(message);
        this.actualMessages
            [message.userPosition]
            [message.position].incoming =
        !this.actualMessages
            [message.userPosition]
            [message.position].incoming;
        this.saveState().then();
    }
    saveState() {
        return this.storage.set('contacts', this.contacts).then(() => {
            this.storage.set('messages', this.messages).then(() => {
                this.storage.set('actualMessages', this.actualMessages).then(() => {
                    this.loadCorrespondence().then();
                });
            });
        });
    }
    loadCorrespondence() {
        return this.storage.get('contacts').then((contacts) => {
            this.contacts = contacts || [];
            this.storage.get('messages').then((messages) => {
                this.messages = messages || [];
                this.storage.get('actualMessages').then((actualMessages) => {
                    this.actualMessages = actualMessages || [];
                });
            });
        });
    }
    clearCorrespondence() {
        this.storage.remove('contacts').then(() => {
            this.storage.remove('messages').then(() => {
                this.storage.remove('actualMessages').then(() => {
                    this.loadCorrespondence();
                });
            });
        });
    }
    getActualMessages() {
        return this.actualMessages;
    }
    getRecords() {
        return this.records;
    }
    clearRecords() {
        this.records = [];
    }
    getTime() {
        return this.time;
    }
    getMessageCount() {
        return this.messagesCount;
    }
    getContacts() {
        return this.contacts;
    }
    getMessages() {
        return this.messages;
    }
    getBackground() {
        return this.background;
    }
    setMessageText(messageText: string) {
        this.messageText = messageText;
    }
    getMessageText() {
        return this.messageText;
    }
}
