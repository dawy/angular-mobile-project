import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {SettingsService} from '../../services/settings.service';
import {PhotoService} from '../../services/photo.service';
import {Record} from '../../model/record';
import {Photo} from '../../model/photo';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  public recipient: {name: string, when: number} = {name: '', when: 0};
  public contact = '';
  public recordLength = 0;
  public messageCount: number;
  public photos: Array<Photo> = [];
  public time: {dHour: number, uHour: number, dMins: number, uMins: number} = {dHour: 0, uHour: 0, dMins: 0, uMins: 0};
  public messageText = '';
  constructor(private router: Router, private settings: SettingsService, private photoService: PhotoService) { }

  ngOnInit() {
    this.messageCount = this.settings.getMessageCount();
    this.photoService.loadPictures().then(() => {
      this.photos = this.photoService.photos;
    });
  }
  callMe() {
    this.router.navigate(['/home/black/screen/', this.recipient.when * 1000, '/home/call/incoming/' + this.recipient.name]).then();
  }
  addContact() {
    this.settings.addContact(this.contact);
  }
  addRecord() {
    this.settings.addRecord(this.recordLength);
  }
  setMessageCont() {
    this.settings.setMessageCount(this.messageCount);
  }
  setMessageText() {
    this.settings.setMessageText(this.messageText);
  }
  setTime() {
    this.settings.setTime(this.time);
  }
  showAllPhotos() {
    this.photoService.showAll();
    this.router.navigateByUrl('/home/gallery');
  }
  removeAllPhotos() {
    this.photoService.removeAll().then(() => {
      this.router.navigateByUrl('/lock');
    });
  }
  trackByFn(index, item) {
    return index;
  }
  clearCorrespondence() {
    this.settings.clearCorrespondence();
  }
}
