import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {BatteryStatus} from '@ionic-native/battery-status/ngx';
import {SettingsService} from '../../services/settings.service';

@Component({
  selector: 'app-lock',
  templateUrl: './lock.page.html',
  styleUrls: ['./lock.page.scss'],
})
export class LockPage implements OnInit {
  statusLevel: any;
  constructor(private router: Router, private batteryStatus: BatteryStatus, public settingsService: SettingsService) { }

  ngOnInit() {
    const subscription = this.batteryStatus.onChange().subscribe(status => {
      console.log(status.level, status.isPlugged);
      this.statusLevel = status.level;
    });
  }

  transitionLandingPage() {
    this.router.navigateByUrl('/home/landing');
  }
}
