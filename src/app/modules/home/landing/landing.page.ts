import { Component, OnInit } from '@angular/core';
import {SettingsService} from '../../../services/settings.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss'],
})
export class LandingPage implements OnInit {
  menuNames: Array<{name: string, path: string}>;
  otherNames: Array<{name: string, path: string}>;
  constructor(private settings: SettingsService) {
    this.menuNames = [
      {
        name: 'call',
        path: '/home/call',
      },
      {
        name: 'camera',
        path: '/home/gallery',
      },
      {
        name: 'text',
        path: '/home/messages',
      },
      {
        name: 'settings',
        path: '/settings',
      }
    ];
    this.otherNames = [
      {
        name: 'browser',
        path: '/lock'
      },
      {
        name: 'calendar',
        path: '/lock'
      },
      {
        name: 'news',
        path: '/lock'
      },
      {
        name: 'run',
        path: '/lock'
      },
      {
        name: 'microphone',
        path: '/home/record',
      },
      {
        name: 'weather',
        path: '/lock'
      }
    ];
  }
  ngOnInit() {
  }

}
