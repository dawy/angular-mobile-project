import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'landing', pathMatch: 'full' },
  { path: 'call', loadChildren: './call/call.module#CallPageModule'},
  { path: 'gallery', loadChildren: './gallery/gallery.module#GalleryPageModule' },
  { path: 'record', loadChildren: './record/record.module#RecordPageModule' },
  { path: 'landing', loadChildren: './landing/landing.module#LandingPageModule' },
  { path: 'black', loadChildren: './black-screen/black-screen.module#BlackScreenPageModule' },
  { path: 'send', loadChildren: './gallery/send/send.module#SendPageModule' },
  { path: 'messages', loadChildren: './messages/messages.module#MessagesPageModule' }



];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
