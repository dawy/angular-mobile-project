import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {SettingsService} from '../../../services/settings.service';

@Component({
  selector: 'app-call',
  templateUrl: './call.page.html',
  styleUrls: ['./call.page.scss'],
})
export class CallPage implements OnInit {
  cell = '';
  constructor(private router: Router, private settings: SettingsService) { }

  ngOnInit() {
  }

  alterNumber(input: string) {
    if (input === 'backspace') {
      this.cell = this.cell.slice(0, this.cell.length - 1);
    } else {
      if (this.cell.length < 9) {
        this.cell = this.cell + input;
      }
    }
  }
  getFormattedCell() {
    if (!isNaN(parseInt(this.cell, 10))) {
      if (this.cell.length <= 2) {
        return this.cell.slice(0, 2);
      } else if (this.cell.length <= 5) {
        return this.cell.slice(0, 2) + '-' + this.cell.slice(2, 5);
      } else if(this.cell.length <= 9) {
        return this.cell.slice(0, 2) + '-' + this.cell.slice(2, 5) + '-' + this.cell.slice(5, 9);
      }
    } else {
      return this.cell;
    }
  }
  call() {
    this.router.navigateByUrl('/home/call/in/' + this.cell);
  }
}
