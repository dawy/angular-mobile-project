import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CallPage} from './call.page';

const routes: Routes = [
  { path: '', component: CallPage },
  { path: 'in/:cell', loadChildren: './in/in.module#InPageModule'},
  { path: 'incoming/:cell', loadChildren: './incoming/incoming.module#IncomingPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class CallRoutingModule { }
