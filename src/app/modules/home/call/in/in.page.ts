import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-in',
  templateUrl: './in.page.html',
  styleUrls: ['./in.page.scss'],
})
export class InPage implements OnInit {
  public active = false;
  public cell = '';
  constructor(private route: ActivatedRoute, private router: Router) {
    this.cell = this.route.snapshot.paramMap.get('cell');
  }

  ngOnInit() {
  }

  getFormattedCell() {
    if (!isNaN(parseInt(this.cell, 10))) {
      if (this.cell.length <= 2) {
        return this.cell.slice(0, 2);
      } else if (this.cell.length <= 5) {
        return this.cell.slice(0, 2) + '-' + this.cell.slice(2, 5);
      } else if(this.cell.length <= 9) {
        return this.cell.slice(0, 2) + '-' + this.cell.slice(2, 5) + '-' + this.cell.slice(5, 9);
      }
    } else {
      return this.cell;
    }
  }
  hangUp() {
    this.router.navigateByUrl('/home/call');
  }
  toggleActive() {
    this.active = !this.active;
  }
}
