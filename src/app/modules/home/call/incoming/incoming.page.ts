import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-in',
  templateUrl: './incoming.page.html',
  styleUrls: ['./incoming.page.scss'],
})
export class IncomingPage implements OnInit {

  cell = '';
  constructor(private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.cell = this.route.snapshot.paramMap.get('cell');
  }

  getFormattedCell() {
    if (!isNaN(parseInt(this.cell, 10))) {
      if (this.cell.length <= 2) {
        return this.cell.slice(0, 2);
      } else if (this.cell.length <= 5) {
        return this.cell.slice(0, 2) + '-' + this.cell.slice(2, 5);
      } else if(this.cell.length <= 9) {
        return this.cell.slice(0, 2) + '-' + this.cell.slice(2, 5) + '-' + this.cell.slice(5, 9);
      }
    } else {
      return this.cell;
    }
  }
  pickUp() {
    this.router.navigateByUrl('/home/call/in/' + this.cell);
  }
  hangUp() {
    this.router.navigateByUrl('/home/call');
  }
}
