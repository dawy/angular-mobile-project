import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CallPage } from './call.page';
import {ComponentsModule} from '../../../components/components.module';
import {CallRoutingModule} from './call-routing.module';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    CallRoutingModule
  ],
  declarations: [CallPage]
})
export class CallPageModule {}
