import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GalleryPage } from './gallery.page';
import {ComponentsModule} from "../../../components/components.module";
import {path} from "@angular-devkit/core";

const routes: Routes = [
  {
    path: '',
    component: GalleryPage
  },
  {
    path: 'send/:photo',
    loadChildren: './send/send.module#SendPageModule'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule
  ],
  declarations: [GalleryPage]
})
export class GalleryPageModule {}
