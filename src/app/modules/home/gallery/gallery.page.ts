import { Component, OnInit } from '@angular/core';
import {PhotoService} from '../../../services/photo.service';
import {Contact} from '../../../model/Contact';
import {Router} from '@angular/router';
import {Photo} from '../../../model/photo';
import {forEach} from '@angular-devkit/schematics';
import {SettingsService} from '../../../services/settings.service';


@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.page.html',
  styleUrls: ['./gallery.page.scss'],
})
export class GalleryPage implements OnInit {
  photos: Photo[] = [];
  sent = false;
  showPopup: boolean;
  showContacts = false;
  step = 1;
  contacts: Array<string> = [];
  content: string;
  test: any;
  popupheight = 0;
  constructor(public photoService: PhotoService, private router: Router, private settings: SettingsService) { }

  ngOnInit() {
    this.contacts = this.settings.getContacts();
    this.showPopup = false;
    this.popupheight = 100;
    this.photoService.loadPictures().then(() => {
      this.photoService.loadPicturesToShow().then(() => {
        this.photos = this.photoService.photosToShow;
      });
    });
  }
  hide() {
    this.step = 1;
  }
  send(photo) {
    console.log(photo);
    // const urlPhoto = photo.replace(/\//g, '*');
    this.test = photo;
    // this.router.navigateByUrl('/home/gallery/send/' + urlPhoto);
  }
  show(photo) {
    this.test = photo;
    this.showPopup = true;
  }
  navigate() {
    const that = this;
    that.sent = true;
    setTimeout(() => {
      that.sent = false;
      that.showContacts = false;
      that.step = 1;
      that.popupheight = 0;
      that.showPopup = false;
      that.router.navigateByUrl('/home/gallery');
    }, 1000);
  }
  handleStep() {
    this.step++;
    this.showContacts = true;
  }
  takePicture() {
    this.photoService.takePicture().then(() => {
      this.photos = this.photoService.photosToShow;
    });
  }
  handleDelete(src) {
    if (confirm('Bist du sicher?')) {
      this.test = null;
      this.showPopup = false;
      this.photoService.hidePhoto(src).then(() => {
        this.photoService.loadPicturesToShow().then(() => {
          this.photos = this.photoService.photosToShow;
        });
      });
    }
  }
  trackByFn(index, item) {
    return index;
  }
  showSentAlert(): boolean {
    return this.sent === true && this.showContacts === true;
  }

}
