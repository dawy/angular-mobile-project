import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Contact} from "../../../../model/Contact";
import {__await} from "tslib";
import {forEach} from "@angular-devkit/schematics";

@Component({
  selector: 'app-send',
  templateUrl: './send.page.html',
  styleUrls: ['./send.page.scss'],
})
export class SendPage implements OnInit {

  photoInput: string;
  photo: any;
  contacts: Contact[] = [];
  sent = false;
  showContacts = false;
  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.photoInput = this.route.snapshot.paramMap.get('photo');
    this.photo = this.photoInput.replace(/\*/g, '/');
    console.log(this.photo);
  }
  back() {
    this.router.navigateByUrl('/home/gallery');
  }
}
