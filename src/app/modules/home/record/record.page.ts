import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-record',
  templateUrl: './record.page.html',
  styleUrls: ['./record.page.scss'],
})
export class RecordPage implements OnInit {
  recordShow: boolean;
  recording: boolean;
  paused: boolean;
  seconds = '';
  constructor(private router: Router) { }

  ngOnInit() {
    this.recordShow = false;
    this.recording = false;
    this.paused = false;
  }
  handleRecord(): void {
    this.recording = true;
    this.paused = false;
    this.recordShow = false;
  }
  handleStop(): void {
    this.recording = false;
    this.paused = false;
  }
  playback(): void {
    this.router.navigateByUrl('/home/record/playback/' + '20');
  }
}
