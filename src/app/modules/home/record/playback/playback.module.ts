import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PlaybackPage } from './playback.page';
import {ComponentsModule} from "../../../../components/components.module";

const routes: Routes = [
    {
        path: '',
        component: PlaybackPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ComponentsModule
    ],
    declarations: [PlaybackPage]
})
export class PlaybackModule {}
