import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-playback',
    templateUrl: './playback.page.html',
    styleUrls: ['./playback.page.scss'],
})
export class PlaybackPage implements OnInit {

    seconds = '';
    constructor(private route: ActivatedRoute, private router: Router) {
        this.seconds = this.route.snapshot.paramMap.get('seconds');
        console.log(this.seconds);
    }

    ngOnInit() {
    }

    back() {
        this.router.navigateByUrl('/home/record');
    }
    getIntSeconds() {
        return parseInt(this.seconds, 10);
    }
}
