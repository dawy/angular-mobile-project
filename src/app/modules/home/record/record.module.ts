import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RecordPage } from './record.page';
import {ComponentsModule} from '../../../components/components.module';


const routes: Routes = [
  {
    path: '',
    component: RecordPage
  },
  {
    path: 'playback/:seconds',
    loadChildren: './playback/playback.module#PlaybackModule'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RecordPage]
})
export class RecordPageModule {}
