import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SettingsService} from '../../../../services/settings.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  contact: { name: string, position: number } = {name: '', position: 0};

  constructor(private activatedRoute: ActivatedRoute, private settings: SettingsService) {
    this.contact.position = parseInt(this.activatedRoute.snapshot.paramMap.get('position'), 10);
    this.contact.name = this.settings.getContacts()[this.contact.position];
  }

  ngOnInit() {

  }

  changeSender(message: { content: string, incoming: boolean, userPosition: number, position: number }) {
    this.settings.changeSender(message);
  }
}
