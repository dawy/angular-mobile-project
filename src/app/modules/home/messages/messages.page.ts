import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {SettingsService} from '../../../services/settings.service';
import {Contact} from '../../../model/Contact';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.page.html',
  styleUrls: ['./messages.page.scss'],
})
export class MessagesPage implements OnInit {
  constructor(private router: Router, private settings: SettingsService) { }

  ngOnInit() {
  }
  openChat(position: number) {
    this.router.navigateByUrl('/home/messages/chat/' + position);
  }

}
