import { Component, OnInit } from '@angular/core';
import {SettingsService} from '../../../services/settings.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-black-screen',
  templateUrl: './black-screen.page.html',
  styleUrls: ['./black-screen.page.scss'],
})
export class BlackScreenPage implements OnInit {
  private sub: any;
  private delay: number;
  private redirectPath: string;
  private timeout: any;
  constructor(private route: ActivatedRoute, private router: Router) {
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      console.log(params.delay);
      console.log(params.redirectPath);
      this.delay = params.delay ? params.delay : 10000;
      this.redirectPath = params.redirectPath ? params.redirectPath : '/lock';
      console.log(this.delay);
      console.log(this.redirectPath);
    });
    this.timeout = setTimeout(() => {
      this.router.navigateByUrl(this.redirectPath).then();
    }, this.delay);
  }

}
