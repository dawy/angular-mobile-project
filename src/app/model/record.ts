import {IonDatetime, IonIcon} from "@ionic/angular";

export class Record {
    length: number;
    size: number;
    icon: string;
    constructor(length: number) {
        this.length = length;
        this.size = length * 3;
        this.icon = 'ios-play';
    }
    getLength() {
        if (this.length >= 60) {
            const seconds = this.length % 60;
            const minutes = (this.length - seconds) / 60;
            if (minutes >= 10 && seconds >= 10) {
                   return minutes + ':' + seconds;
               } else if (minutes >= 10 && seconds < 10) {
                   return minutes + ':0' + seconds;
               } else if (minutes < 10 && seconds >= 10) {
                   return '0' + minutes + ':' + seconds;
               } else if (minutes < 10 && seconds < 10) {
                   return '0' + minutes + ':' + '0' + seconds;
               }
            } else {
                if (this.length >= 10) {
                    return '00:' + this.length;
                } else {
                    return '00:' + '0' + this.length;
                }
            }
    }
    changeIcon() {
        if (this.icon === 'ios-play') {
            this.icon = 'square';
        } else {
            this.icon = 'ios-play';
        }
    }
}
