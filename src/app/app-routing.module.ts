import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/lock', pathMatch: 'full' },
  { path: 'home', loadChildren: './modules/home/home.module#HomePageModule' },
  { path: 'lock', loadChildren: './modules/lock/lock.module#LockPageModule' },
  { path: 'settings', loadChildren: './modules/settings/settings.module#SettingsPageModule' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
