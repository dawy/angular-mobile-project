import { Component, OnInit } from '@angular/core';
import {Record} from '../../model/record';
import {Router} from '@angular/router';
import {SettingsService} from "../../services/settings.service";

@Component({
  selector: 'app-records-list',
  templateUrl: './records-list.component.html',
  styleUrls: ['./records-list.component.scss'],
})
export class RecordsListComponent implements OnInit {
  records: Array<Record>;
  countRecords: number;
  playing: boolean;
  minutes: number;
  constructor(private router: Router, private settings: SettingsService) { }

  ngOnInit() {
    this.records = this.settings.getRecords();
    this.countRecords = this.CountRecords();
    this.playing = false;
  }
  CountRecords(): number {
    let i = 0;
    for (const rec of this.records) {
      i++;
    }
    return i;
  }
  play(seconds) {
    const newSeconds = parseInt(seconds, 10);
    this.router.navigateByUrl('/home/record/playback/' + newSeconds);
  }

}
