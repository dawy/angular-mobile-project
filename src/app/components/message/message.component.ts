import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {
  @Input() text = 'default';
  @Input() floatLeft = true;
  float = '';
  constructor() { }

  ngOnInit() {
    if (this.floatLeft) {
      this.float = 'left';
    } else {
      this.float = 'right';
    }
  }
}
