import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-status-bar',
  templateUrl: './status-bar.component.html',
  styleUrls: ['./status-bar.component.scss'],
})
export class StatusBarComponent implements OnInit {
  @Input() routeBack: string;
  constructor(private router: Router) {
  }

  ngOnInit() {}

  goHome() {
    this.router.navigateByUrl('/lock');
  }
  goBack() {
    this.router.navigateByUrl(this.routeBack);
  }
}
