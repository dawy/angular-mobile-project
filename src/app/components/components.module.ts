import { WaveformComponent } from './waveform/waveform.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import {StatusBarComponent} from './status-bar/status-bar.component';
import {RecordsListComponent} from './records-list/records-list.component';
import {RecordAnimateComponent} from './record-animate/record-animate.component';
import {LoadingBarComponent} from './loading-bar/loading-bar.component';
import {ClockComponent} from './clock/clock.component';
import {SendMessageComponent} from './send-message/send-message.component';
import {MessageComponent} from "./message/message.component";




@NgModule({
    imports: [CommonModule, FormsModule, IonicModule],
    declarations: [
        StatusBarComponent,
        RecordsListComponent,
        RecordAnimateComponent,
        WaveformComponent,
        LoadingBarComponent,
        ClockComponent,
        SendMessageComponent,
        MessageComponent
    ],
    exports: [
        StatusBarComponent,
        RecordsListComponent,
        RecordAnimateComponent,
        WaveformComponent,
        LoadingBarComponent,
        ClockComponent,
        SendMessageComponent,
        MessageComponent],
})
export class ComponentsModule {}
