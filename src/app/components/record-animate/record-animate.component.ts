import {Component, Input, OnInit, OnDestroy} from '@angular/core';
import { timer } from 'rxjs';

@Component({
  selector: 'app-record-animate',
  templateUrl: './record-animate.component.html',
  styleUrls: ['./record-animate.component.scss'],
})
export class RecordAnimateComponent implements OnInit, OnDestroy {
  @Input() recording: boolean;
  @Input() paused: boolean;
  @Input() forDownCount: string;
  @Input() color: string;
  forDownCountNumber: number;
  @Input() playback: boolean;
  seconds: number;
  minutes: number;
  minuteShowZeroes: boolean;
  secondShowZeroes: boolean;
  aBigNumba: number;
  subscription;
  constructor() {}
  ngOnInit() {
    this.subscription = timer(0, 1000).subscribe(value => this.handleTimer());
    this.seconds = 0;
    this.minutes = 0;
    this.minuteShowZeroes = true;
    this.secondShowZeroes = true;
    if (this.forDownCount === undefined) {
      this.forDownCount = '';
      this.forDownCountNumber = 0;
    } else {
      this.forDownCountNumber = +this.forDownCount;
    }
    if (this.playback === undefined) {
      this.playback = false;
    }
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  handleTimer() {
    if (this.playback === false) {
      if (this.recording === false) {
        this.seconds = 0;
        this.minutes = 0;
        this.secondShowZeroes = true;
        this.minuteShowZeroes = true;
      } else if (this.recording && !this.paused) {
        this.seconds++;
        if (this.seconds >= 60) {
          this.seconds = 0;
          this.minutes++;
        }
        this.handleShowZeroes();
      }
    } else {
      if (this.forDownCountNumber >= 0) {
        if (this.forDownCountNumber >= 60 ) {
          this.seconds = Math.floor(this.forDownCountNumber % 60);
          this.minutes = Math.floor(this.forDownCountNumber / 60);
        } else {
          this.minutes = 0;
          this.seconds = this.forDownCountNumber;
        }
        this.forDownCountNumber--;
      }
      this.handleShowZeroes();
    }


  }
  downCount() {

  }
  handleMinuteShowZeroes() {
    this.minuteShowZeroes = this.minutes < 10;
  }
  handleSecondsShowZeroes() {
    this.secondShowZeroes = this.seconds < 10;
  }
  handleShowZeroes() {
    this.handleMinuteShowZeroes();
    this.handleSecondsShowZeroes();
  }
}
