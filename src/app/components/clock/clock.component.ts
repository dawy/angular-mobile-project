import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss'],
})
export class ClockComponent implements OnInit {
  @Input() time: {dHour: number, uHour: number, dMins: number, uMins: number};
  @Input() color: string;
  @Input() sizeVh: number;
  constructor() { }

  ngOnInit() {
  }

}
