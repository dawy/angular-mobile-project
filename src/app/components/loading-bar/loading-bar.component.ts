import {Component, Input, OnInit} from '@angular/core';
import {timer} from "rxjs";

@Component({
  selector: 'app-loading-bar',
  templateUrl: './loading-bar.component.html',
  styleUrls: ['./loading-bar.component.scss'],
})
export class LoadingBarComponent implements OnInit {
  @Input() seconds: number;
  iterator: number;
  width: string;
  subscription;
  constructor() { }

  ngOnInit() {
    this.subscription = timer(0, 1000).subscribe(value => this.handleBar());
    this.iterator = 1;
    this.width = '5';
    console.log(this.seconds);
  }

  handleBar(): void {
    if (this.iterator <= this.seconds) {
      this.width = 'calc(' + this.iterator + ' * 100% / ' + this.seconds + ')';
      this.iterator++;
      console.log(this.width);
    } else {
      this.subscription.unsubscribe();
    }
  }

}
