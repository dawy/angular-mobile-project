import {Component, Input, OnInit} from '@angular/core';
import {SettingsService} from '../../services/settings.service';

@Component({
  selector: 'app-send-message',
  templateUrl: './send-message.component.html',
  styleUrls: ['./send-message.component.scss'],
})
export class SendMessageComponent implements OnInit {
  @Input() contact: { name: string, position: number };
  message = '';
  constructor(private settings: SettingsService) { }

  ngOnInit() {}
  sendMessage() {
    this.settings.addActualMessage({content: this.message, incoming: false, userPosition: this.contact.position});
  }
}
